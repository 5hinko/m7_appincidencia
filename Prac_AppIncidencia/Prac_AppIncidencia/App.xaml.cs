﻿using Prac_AppIncidencia.Service;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace Prac_AppIncidencia
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();


            //nombre, apellido, correo, pass
            ListUsers.newUsuario("Xin", "cho", "1234", "1234");

            //(int equipID, int usuID, float progres, string nombre, string origen, string descr, DateTime dateCreat)
            ListIncidencia lista = new ListIncidencia();
            lista.newIncidencia(28199, 10, 50, "Reparar cablejat", "Professor", "Lorem Ipsum, Lorem Ipsum", new DateTime(2018, 2, 26));
            lista.newIncidencia(34579, 11, 100, "Pantalla no rep", "Estudiant", "Lorem Ipsum, Lorem Ipsum", new DateTime(2018, 6, 27));
            lista.newIncidencia(28199, 23, 0, "Reparar punt de xarxa", "Estudiant", "Lorem Ipsum, Lorem Ipsum", new DateTime(2018, 2, 26));
            lista.newIncidencia(56790, 01, 30, "Pantalla no rep", "Estudiant", "Lorem Ipsum, Lorem Ipsum", new DateTime(2018, 6, 29));
            lista.newIncidencia(43567, 12, 100, "Reparar cablejat", "Professor", "Lorem Ipsum, Lorem Ipsum", new DateTime(2018, 10, 26));
            lista.newIncidencia(77554, 23, 100, "Pantalla no rep senyal", "Professor", "Lorem Ipsum, Lorem Ipsum", new DateTime(2018, 11, 5));
            lista.newIncidencia(28199, 10, 50, "Reparar cablejat", "Professor", "Lorem Ipsum, Lorem Ipsum", new DateTime(2018, 2, 26));
            lista.newIncidencia(34579, 11, 100, "Pantalla no rep", "Estudiant", "Lorem Ipsum, Lorem Ipsum", new DateTime(2018, 6, 27));
            lista.newIncidencia(28199, 23, 0, "Reparar punt de xarxa", "Estudiant", "Lorem Ipsum, Lorem Ipsum", new DateTime(2018, 2, 26));
            lista.newIncidencia(56790, 01, 30, "Pantalla no rep", "Estudiant", "Lorem Ipsum, Lorem Ipsum", new DateTime(2018, 6, 29));
            lista.newIncidencia(43567, 12, 100, "Reparar cablejat", "Professor", "Lorem Ipsum, Lorem Ipsum", new DateTime(2018, 10, 26));
            lista.newIncidencia(77554, 23, 100, "Pantalla no rep senyal", "Professor", "Lorem Ipsum, Lorem Ipsum", new DateTime(2018, 11, 5));


            MainPage = new NavigationPage(new LoginPage());
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
