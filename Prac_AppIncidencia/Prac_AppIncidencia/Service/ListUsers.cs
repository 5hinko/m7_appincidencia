﻿using Prac_AppIncidencia.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Prac_AppIncidencia.Service
{
    class ListUsers
    {
       static List<User> usuarios = new List<User>();
        static User log = null;

        private static User buscaUsuario(String usuStr)
        {
            foreach (User user in usuarios)
            {
                String asd = (String)user.Correo;
                if (user.Correo == usuStr)
                {
                    return user;
                }
            }
            return null;
        }

        public static Boolean existeUsuario(String usuStr)
        {
            return (buscaUsuario(usuStr) != null);
        }

        public static User getUsuario(String usuStr, String pass)
        {
            User usuario = buscaUsuario(usuStr);

            if (usuario != null)
            {
                if (usuario.Pass == pass)
                {
                    return usuario;
                }
                return null;
            }
            else
            {
                return null;
               
            }

            
        }

        public static User getLogUser()
        {
            return log;
        }

        public static Boolean newUsuario(string nombre, string apellido, string correo, string pass)
        {
            if (!existeUsuario(correo))
            {
                User us = new User(nombre, apellido, correo, pass);
                usuarios.Add(us);
                return true;
            }
            else
            {
                return false;
            }
        }

        public static Boolean alguienLog()
        {
            return (log != null);
        }

        public static Boolean login (String usu , String pass)
        {
            if(!alguienLog())
            {
                if (existeUsuario(usu))
                {
                    if (getUsuario(usu, pass) != null)
                    {
                        log = (getUsuario(usu, pass));
                        return true;
                    }
                }
            }
            return false;
        }
        public static void logout()
        {
            log = null;
        }
    }
}
