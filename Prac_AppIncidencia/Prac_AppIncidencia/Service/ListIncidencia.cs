﻿using Prac_AppIncidencia.Models;
using System;
using System.Collections.Generic;
using System.Text;


namespace Prac_AppIncidencia.Service
{
    class ListIncidencia
    {
        private static List<Incidencia> listaInci = new List<Incidencia>();
        
        public ListIncidencia()
        {

        }

        public void newIncidencia( int equipID, int usuID, float progres, string nombre, string origen, string descr, DateTime dateCreat)
        {
            Incidencia inc = new Incidencia(equipID, usuID, progres, nombre, origen, descr, dateCreat);
            listaInci.Add(inc);
        }

        public List<Incidencia> getLista()
        {
            return listaInci;
        }

        public void deleteIncidencia(int uID)
        {
            Incidencia asd = showIncidencia(uID);
            listaInci.Remove(asd);
        }

        public Incidencia showIncidencia(int uID)
        {
            foreach (var asd in listaInci)
            {
                if (asd.UID.Equals(uID))
                {
                    return asd;
                }
            }
            return null;
        }
        
    }
}
