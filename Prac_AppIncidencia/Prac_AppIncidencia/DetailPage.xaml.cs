﻿using Prac_AppIncidencia.Models;
using Prac_AppIncidencia.Service;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Prac_AppIncidencia
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DetailPage : MasterDetailPage
    {
        Incidencia item;

        public DetailPage()
        {
            InitializeComponent();

            if (!ListUsers.alguienLog()) new LoginPage();

            MasterPage.ListView.ItemSelected += ListView_ItemSelected;
        }

        private void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {

            //var item = e.SelectedItem as Incidencia;
            item = e.SelectedItem as Incidencia;

            if (item == null)
                return;


            //var page = (Page)Activator.CreateInstance(item.TargetType);
            
            var page = (new DetailPageDetail(item));
            page.Title = item.Nombre;
            page.Nombre = item.Nombre;
            page.IncidenciaMostra = item;
            Detail = new NavigationPage(page);

            //Detail = new NavigationPage(new DetailPageDetail(item));

            IsPresented = false;
            MasterPage.ListView.SelectedItem = null;
        }
    }
}