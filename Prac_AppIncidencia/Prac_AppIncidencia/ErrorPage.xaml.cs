﻿using Prac_AppIncidencia.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Prac_AppIncidencia
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ErrorPage : ContentPage
	{
		public ErrorPage ()
		{
			InitializeComponent ();
            if (!ListUsers.alguienLog()) new LoginPage();

        }
        private async void btn_retornaClicked(object sender, EventArgs e) {
            await Navigation.PopModalAsync();
        }
	}
}