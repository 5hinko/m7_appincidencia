﻿using Prac_AppIncidencia.Service;
using System;
using Xamarin.Forms;

namespace Prac_AppIncidencia
{
    public partial class LoginPage : ContentPage
    {

        Image image;
        public LoginPage()
        {
            this.InitializeComponent();

            if (ListUsers.alguienLog()) Navigation.PushAsync(new WellcomePage());

            
            image = new Image { Source = "iconPrincipal.jpg" };
            image.Source = Device.RuntimePlatform == Device.Android ? ImageSource.FromFile("iconPrincipal.jpg") : ImageSource.FromFile("Img/iconPrincipal.jpg");
        }


        public void OnEnterPressed(object sender, EventArgs e)
        {
            btn_accederClicked(sender, null);
        }

        private async void btn_accederClicked(object sender, EventArgs e)
        {
            
            try
            {
                var usu = txtUsu.Text;
                var pass = txtPass.Text;
                if (usu.Length > 0 && pass.Length > 0)
                {
                    if (ListUsers.login(usu, pass))
                    {
                        await Navigation.PushAsync(new WellcomePage());
                    }
                    else
                    {
                        await Navigation.PushModalAsync(new ErrorPage());
                    }
                }
            }catch (Exception exc)
            {
                await Navigation.PushModalAsync(new ErrorPage());
            }
        }

        public void btn_registraClicked (object sender, EventArgs e)
        {
            
        }
    }
}
