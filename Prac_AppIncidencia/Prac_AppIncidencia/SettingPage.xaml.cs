﻿using Prac_AppIncidencia.Models;
using Prac_AppIncidencia.Service;
using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Prac_AppIncidencia
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SettingPage : ContentPage
	{

        public string nombre { get; set; }
        public string correo { get; set; }

        public SettingPage ()
		{
			InitializeComponent ();
            if (!ListUsers.alguienLog()) new LoginPage();

            User usuario = ListUsers.getLogUser();

            uid.Text = (usuario.IdUs).ToString();
            nombre = usuario.Nombre;
            sign.Text = usuario.Apellido;
            correo = usuario.Correo;

            BindingContext = this;
            
		}

        public void btnLogoutClicked(object sender, EventArgs e)
        {
            ListUsers.logout();
            new LoginPage();
        }
	}
}