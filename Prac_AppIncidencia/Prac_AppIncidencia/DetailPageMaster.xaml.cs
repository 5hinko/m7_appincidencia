﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Prac_AppIncidencia.Models;
using Prac_AppIncidencia.Service;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Prac_AppIncidencia
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DetailPageMaster : ContentPage
    {
        public ListView ListView;

        public DetailPageMaster()
        {
            InitializeComponent();

            if (!ListUsers.alguienLog()) new LoginPage();

            BindingContext = new DetailPageMasterViewModel();
            ListView = MenuItemsListView;
            
        }

        class DetailPageMasterViewModel : INotifyPropertyChanged
        {
            public ObservableCollection<Incidencia> MenuItems { get; set; }
            
            public DetailPageMasterViewModel()
            {
                MenuItems = new ObservableCollection<Incidencia> { };
               // MenuItems = new ListIncidencia().getLista();
                
                foreach (var lista in new ListIncidencia().getLista()){
                    MenuItems.Add(new Incidencia
                    {
                        UID = lista.UID,
                        EquipID = lista.EquipID,
                        UsuID = lista.UsuID,
                        Progres = lista.Progres,
                        Nombre = lista.Nombre,
                        Origen = lista.Origen,
                        Descr = lista.Descr,
                        DateCreat = lista.DateCreat
                    });
                }
            }
            
            #region INotifyPropertyChanged Implementation
            public event PropertyChangedEventHandler PropertyChanged;
            void OnPropertyChanged([CallerMemberName] string propertyName = "")
            {
                if (PropertyChanged == null)
                    return;

                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
            #endregion
        }
    }
}