﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prac_AppIncidencia
{

    public class DetailPageMenuItem
    {
        public DetailPageMenuItem()
        {
            TargetType = typeof(DetailPageDetail);
        }

        public DateTime DateCreat { get; set; }
        public string Nombre { get; set; }
        public string Origen { get; set; }
        public string Descr { get; set; }
        public float Progres { get; set; }
        public int UID { get; set; }
        public int EquipID { get; set; }
        public int UsuID { get; set; }
        public Type TargetType { get; set; }
    }
}