﻿using Prac_AppIncidencia.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Prac_AppIncidencia
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class WellcomePage : TabbedPage
    {
        public WellcomePage()
        {
            InitializeComponent();
            if (!ListUsers.alguienLog()) new LoginPage();

        }
    }
}