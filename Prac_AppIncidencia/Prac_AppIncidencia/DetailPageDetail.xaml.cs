﻿using Prac_AppIncidencia.Models;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Prac_AppIncidencia
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DetailPageDetail : ContentPage
    {
        public Incidencia IncidenciaMostra { get; set; }
        public string Nombre { get; set; }

        public DetailPageDetail()
        {
            InitializeComponent();

        }

        public DetailPageDetail(Incidencia incidencia)
        {
            //if (!ListUsers.alguienLog()) new LoginPage();

            InitializeComponent();
            IncidenciaMostra = incidencia;

            if(IncidenciaMostra != null)
            {
                //Title = IncidenciaMostra.Nombre;
                //Nombre = IncidenciaMostra.Nombre;

                DateCreat.Text = IncidenciaMostra.DateCreat.ToString();
                //Nombre.Text = IncidenciaMostra.Nombre;
                Origen.Text = IncidenciaMostra.Origen;
                Descr.Text = IncidenciaMostra.Descr;
                Progres.Text = IncidenciaMostra.Progres.ToString();
                UID.Text = IncidenciaMostra.UID.ToString();
                EquipID.Text = IncidenciaMostra.EquipID.ToString();
                UsuID.Text = IncidenciaMostra.UsuID.ToString();

                Console.Write(IncidenciaMostra.Nombre);
            }

        }
        
    }
}