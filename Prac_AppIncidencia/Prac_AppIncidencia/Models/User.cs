﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Prac_AppIncidencia.Models
{
    class User
    {
        private int idUs;
        private String nombre, apellido, correo, pass;

        private static int autoIncre = 0;
        public User(string nombre, string apellido, string correo, string pass)
        {
            idUs = autoIncre;
            this.nombre = nombre;
            this.apellido = apellido;
            this.correo = correo;
            this.pass = pass;


            autoIncre++;
        }

        public int IdUs { get => idUs; set => idUs = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string Apellido { get => apellido; set => apellido = value; }
        public string Correo { get => correo; set => correo = value; }
        public string Pass { get => pass; set => pass = value; }
        
    }
}
