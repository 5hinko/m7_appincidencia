﻿using System;

namespace Prac_AppIncidencia.Models
{
    public class Incidencia
    {
        private static int autoIncre = 1;

        public DateTime DateCreat { get ; set ; }
        public string Nombre { get; set; }
        public string Origen { get; set; }
        public string Descr { get; set; }
        public float Progres { get; set; }
        public int UID { get; set; }
        public int EquipID { get; set; }
        public int UsuID { get; set; }

        public Type TargetType { get; set; }

        public Incidencia()
        {

            TargetType = typeof(DetailPageDetail);
        }

        public Incidencia(int equipID, int usuID, float progres, string nombre, string origen, string descr, DateTime dateCreat)
        {
            this.UID = autoIncre;
            this.EquipID = equipID;
            this.UsuID = usuID;
            this.Progres = progres;
            this.Nombre = nombre;
            this.Origen = origen;
            this.Descr = descr;
            this.DateCreat = dateCreat;

            autoIncre++;
        }

        public Incidencia(int uID, int equipID, int usuID, float progres, string nombre, string origen, string descr, DateTime dateCreat)
        {
            this.UID = uID;
            this.EquipID = equipID;
            this.UsuID = usuID;
            this.Progres = progres;
            this.Nombre = nombre;
            this.Origen = origen;
            this.Descr = descr;
            this.DateCreat = dateCreat;
            //String.Format("{0:dd/MM/yy}", DateCreat)
        }
    }
}
